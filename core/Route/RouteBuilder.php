<?php

namespace Core\Route;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use \Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;

class RouteBuilder
{
    /**
     * @var \Symfony\Component\Routing\RequestContext
     */
    private $requestContext;

    public function __construct(RequestContext $requestContext)
    {
        $this->requestContext = $requestContext;
    }

    public function urlMatcher()
    {
        $loader = new YamlFileLoader(new FileLocator(__DIR__));
        return new UrlMatcher($loader->load('../../src/routes.yml'), $this->requestContext);
    }
}
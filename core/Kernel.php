<?php

namespace Core;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader as DIYamlFileLoader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\HttpKernel\EventListener\RouterListener;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\TerminableInterface;
use Symfony\Component\Routing\Loader\YamlFileLoader as RYamlFileLoader;

class Kernel implements TerminableInterface, HttpKernelInterface
{

    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    private $request;

    /**
     * @var \Symfony\Component\HttpKernel\HttpKernel
     */
    private $httpKernel;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public static function createGlobalHttp()
    {
        return new static(Request::createFromGlobals());
    }

    public function sendResponse()
    {
        $response = $this->handle($this->request);
        $response->send();
        $this->terminate($this->request, $response);
    }

    /**
     * {@inheritdoc}
     */
    public function terminate(Request $request, Response $response)
    {
        $this->httpKernel->terminate($request, $response);
    }

    /**
     * {@inheritdoc}
     */
    public function handle(Request $request, int $type = self::MASTER_REQUEST, bool $catch = true)
    {
        $service = new ContainerBuilder();
        $loader = new DIYamlFileLoader($service, new FileLocator(__DIR__));
        $loader->load('../src/services.yml');

        $dispatcher = $service->get('event.dispatcher');
        $dispatcher
          ->addSubscriber(new RouterListener($service->get('route.builder')->urlMatcher(), new RequestStack()));

        $controllerResolver = new ControllerResolver();
        $argumentResolver = new ArgumentResolver();
        $this->httpKernel = new HttpKernel($dispatcher, $controllerResolver, new RequestStack(), $argumentResolver);

        return $this->httpKernel->handle($request);
    }

}
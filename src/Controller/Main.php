<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;

class Main
{

    public function front()
    {
        $loader = new \Twig\Loader\FilesystemLoader('./templates');
        $twig = new \Twig\Environment($loader, [
          'cache' => '/var/www/html/cache',
        ]);

        return new Response($twig->render('index.twig', ['name' => 'Fabien']));
    }

}